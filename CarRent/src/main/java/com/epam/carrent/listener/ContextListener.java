package com.epam.carrent.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import com.epam.carrent.dao.DaoFactory;
import com.epam.carrent.dao.impl.DaoFactoryException;
import com.epam.carrent.dao.impl.MySqlDaoFactory;

public class ContextListener implements ServletContextListener {

	private static final Logger log = Logger.getLogger(ContextListener.class);
	
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		log.info("context initialized");
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		@SuppressWarnings("rawtypes")
		DaoFactory factory = MySqlDaoFactory.getInstance();
		try {
			factory.releaseConnections();
		} catch (DaoFactoryException e) {
			log.error("DaoFactoryException at context destroyed");
		}

	}
}
