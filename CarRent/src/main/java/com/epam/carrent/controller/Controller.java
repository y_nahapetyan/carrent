package com.epam.carrent.controller;

import org.apache.log4j.Logger;

import com.epam.carrent.action.Action;
import com.epam.carrent.action.ActionException;
import com.epam.carrent.action.ActionFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(Controller.class);
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) 
    		throws ServletException, IOException {
        log.info(request);
        doAction(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) 
    		throws ServletException, IOException {
        log.info(request);
        doAction(request, response);
    }

    protected void doAction(HttpServletRequest request,
    	    HttpServletResponse response) throws ServletException, IOException {
    	Action action;
    	action = ActionFactory.getAction(request);
    	try { 
    	   action.execute(request, response);
    	} catch (ActionException e) {
    		log.error(e);
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/404" + ".tiles");
            requestDispatcher.forward(request, response);
    	}
    }
}
