package com.epam.carrent.dao.impl.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

import com.epam.carrent.entity.CarFilter;

public class CarFilterQueryBuilder {
  private static  ResourceBundle langResourceBundle;	
    
private static final String FILTER_DATE_CONDITION1 = "(date_start <= ? AND  date_end>= ?)";
private static final String FILTER_DATE_CONDITION2 = "(date_start>= ? AND date_start <= ?)";
private static final String CLASS_NAME = "class_name";
private static final String HAS_CONDITION = "has_condition";
private static final String IS_AUTOMAT = "is_automat";
private static final String IS_DIESEL = "is_diesel";
private static final String PRICE = "price";
private static final String CAR_ID = "cars.id";
private static final String CARS_CAR_ID = "cars.id";
private static final String DATE_ORDER_FILTER =  "select orders.car_id from orders \n" +
						"join cars on orders.car_id = cars.id \n"+
						"where orders.status <> 'rejected' and (";
	
	public static StringBuilder getAvailableStatement(int carId, String beginDate, String endDate){
	    StringBuilder preparedStatementBuilder = new StringBuilder();
	    preparedStatementBuilder.append(DATE_ORDER_FILTER);
	    preparedStatementBuilder.append(" " + CARS_CAR_ID + " =? ");
	    preparedStatementBuilder.append(" and (" + FILTER_DATE_CONDITION1);
	    preparedStatementBuilder.append(" or " + FILTER_DATE_CONDITION2 + "))" );
	    return preparedStatementBuilder;
	}

    public static DateFilterStatus createFilterCriteria(CarFilter carFilter, StringBuilder preparedStatementBuilder) {
        boolean isFirstStatementAdded = createFilterExceptDate(carFilter, preparedStatementBuilder);
        return createDateFilter(carFilter, preparedStatementBuilder, isFirstStatementAdded);
    }

    public static String createEndDateSubstitute(SimpleDateFormat simpleDateFormat) {
        Date endDateSubstitute = new Date();
        endDateSubstitute.setYear(endDateSubstitute.getYear() + 100);
        return simpleDateFormat.format(endDateSubstitute);
    }

    private static DateFilterStatus createDateFilter(CarFilter carFilter, StringBuilder preparedStatementBuilder, boolean isFirstStatementAdded) {
        DateFilterStatus dateFilterStatus = DateFilterStatus.BOTH_NULL;
        if (!carFilter.getBeginDate().equals("")){
            isFirstStatementAdded = setStatementStart(preparedStatementBuilder, isFirstStatementAdded);
            dateFilterStatus = DateFilterStatus.BEGIN_SET;
            preparedStatementBuilder.append(" " + CAR_ID + " NOT IN (");
            preparedStatementBuilder.append(DATE_ORDER_FILTER);
            preparedStatementBuilder.append(FILTER_DATE_CONDITION1);
            preparedStatementBuilder.append(" OR ");
            preparedStatementBuilder.append(FILTER_DATE_CONDITION2);
            preparedStatementBuilder.append(")");
        }
        if (!carFilter.getEndDate().equals("")){
            if (dateFilterStatus == DateFilterStatus.BOTH_NULL) {
                isFirstStatementAdded = setStatementStart(preparedStatementBuilder, isFirstStatementAdded);
                dateFilterStatus = DateFilterStatus.END_SET;
                preparedStatementBuilder.append(" " + CAR_ID + " NOT IN (");
                preparedStatementBuilder.append(DATE_ORDER_FILTER);
                preparedStatementBuilder.append(FILTER_DATE_CONDITION2);
                preparedStatementBuilder.append(")");
            } else {
                dateFilterStatus = DateFilterStatus.BEGIN_END_SET;
            }
        }
        if (dateFilterStatus != DateFilterStatus.BOTH_NULL) preparedStatementBuilder.append(")");
        return dateFilterStatus;
    }

    private static boolean createFilterExceptDate(CarFilter carFilter, StringBuilder preparedStatementBuilder) {
        langResourceBundle = ResourceBundle.getBundle("language");
        String ANY = langResourceBundle.getString("ANY");
        String WITH_CONDITION = langResourceBundle.getString("WITH_CONDITION");
        String AUTOMAT = langResourceBundle.getString("AUTOMAT");
        String DIESEL = langResourceBundle.getString("DIESEL");

        boolean isFirstStatementAdded = false;
        isFirstStatementAdded = addClassFilter(carFilter.getClassName(), preparedStatementBuilder, ANY, isFirstStatementAdded);
        isFirstStatementAdded = addConditionFilter(carFilter.getHasCondition(), preparedStatementBuilder, ANY, WITH_CONDITION, isFirstStatementAdded);
        isFirstStatementAdded = addTransmissionFilter(carFilter.getIsAutomat(), preparedStatementBuilder, ANY, AUTOMAT, isFirstStatementAdded);
        isFirstStatementAdded = addFuelFilter(carFilter.getIsDiesel(), preparedStatementBuilder, ANY, DIESEL, isFirstStatementAdded);
        isFirstStatementAdded = addPriceFilter(carFilter.getPrice(), preparedStatementBuilder, isFirstStatementAdded);

        return isFirstStatementAdded;
    }
    
    private static boolean addPriceFilter(String priceString, StringBuilder preparedStatementBuilder, boolean isFirstStatementAdded) {
        if (!priceString.equals("")){
            isFirstStatementAdded = setStatementStart(preparedStatementBuilder, isFirstStatementAdded);
            preparedStatementBuilder.append(" " + PRICE +" <= "+ priceString);
        }
        return isFirstStatementAdded;
    }

    private static boolean addFuelFilter(String carFuel, StringBuilder preparedStatementBuilder, String ANY, String DIESEL, boolean isFirstStatementAdded) {
        if (!carFuel.equals(ANY)){
            isFirstStatementAdded = setStatementStart(preparedStatementBuilder, isFirstStatementAdded);
            preparedStatementBuilder.append(" " + IS_DIESEL + " = " + carFuel.equals(DIESEL));
        }
        return isFirstStatementAdded;
    }

    private static boolean addTransmissionFilter(String carTransmission, StringBuilder preparedStatementBuilder, String ANY, String AUTOMAT, boolean isFirstStatementAdded) {
        if (!carTransmission.equals(ANY)){
            isFirstStatementAdded = setStatementStart(preparedStatementBuilder, isFirstStatementAdded);
            preparedStatementBuilder.append(" " + IS_AUTOMAT + " = " + carTransmission.equals(AUTOMAT));
        }
        return isFirstStatementAdded;
    }

    private static boolean addConditionFilter(String carCondition, StringBuilder preparedStatementBuilder, String ANY, String WITH_CONDITION, boolean isFirstStatementAdded) {
        if (!carCondition.equals(ANY)){
            isFirstStatementAdded = setStatementStart(preparedStatementBuilder, isFirstStatementAdded);
            preparedStatementBuilder.append(" " + HAS_CONDITION + " = " + carCondition.equals(WITH_CONDITION));
        }
        return isFirstStatementAdded;
    }

    private static boolean addClassFilter(String carClass, StringBuilder preparedStatementBuilder, String ANY, boolean isFirstStatementAdded) {
        if (!carClass.equals(ANY)){
            isFirstStatementAdded = setStatementStart(preparedStatementBuilder, isFirstStatementAdded);
            preparedStatementBuilder.append(" " + CLASS_NAME + " = '"+ carClass + "'");
        }
        return isFirstStatementAdded;
    }

    private static boolean setStatementStart(StringBuilder preparedStatementBuilder, boolean isFirstStatementAdded) {
        if (!isFirstStatementAdded) {
            isFirstStatementAdded = true;
            preparedStatementBuilder.append(" where ");
        } else {
            preparedStatementBuilder.append(" and ");
        }
        return isFirstStatementAdded;
    }
}
