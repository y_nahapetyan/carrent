package com.epam.carrent.dao.impl.util;

public enum DateFilterStatus implements FilterStatus{
    BEGIN_END_SET,
    BEGIN_SET,
    END_SET,
    BOTH_NULL
}
