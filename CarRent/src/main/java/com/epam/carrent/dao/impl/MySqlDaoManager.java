package com.epam.carrent.dao.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.epam.carrent.dao.DaoManager;
import com.epam.carrent.dao.GenericDao;
import com.epam.carrent.dao.PersistException;
import com.epam.carrent.dao.DaoFactory.DaoCreator;
import com.epam.carrent.entity.Car;
import com.epam.carrent.entity.Order;
import com.epam.carrent.entity.User;
	
public class MySqlDaoManager implements DaoManager{
    private Connection connection = null;
	private Map<Class, DaoCreator> creators;
	
    public MySqlDaoManager(Connection connection){
        this.connection = connection;
        
        creators = new HashMap<Class, DaoCreator>();
        creators.put(User.class, new DaoCreator<Connection>() {
            @Override
            public GenericDao create(Connection connection) {
                return new UserDao(connection);
            }
        });
        creators.put(Car.class, new DaoCreator<Connection>() {
            @Override
            public GenericDao create(Connection connection) {
                return new CarDao(connection);
            }
        });
        creators.put(Order.class, new DaoCreator<Connection>() {
            @Override
            public GenericDao create(Connection connection) {
                return new OrderDao(connection);
            }
        });
    }

    public void beginTransaction() throws PersistException {
        try {
            connection.setAutoCommit(false);
        } catch (SQLException e) {
        	throw new PersistException(e);
        }
    }

    public void commit() throws PersistException {
        try {
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
        	throw new PersistException(e);
        }
    }

    public void rollback() throws PersistException {
        try {
            connection.rollback();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
        	throw new PersistException(e);
        }
    }

    public void closeConnection() throws PersistException{
        try {
            connection.close();
        } catch (SQLException e) {
        	throw new PersistException(e);
        }

    }
    
	public GenericDao getDao(Class clazz) throws PersistException{
		DaoCreator creator = creators.get(clazz);
    	if (creator == null) {
    		throw new PersistException("Dao object for " + clazz + " not found.");
    	}
    	return creator.create(connection);
	}
}
