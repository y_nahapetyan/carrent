package com.epam.carrent.dao;

public interface DaoManager {
    public abstract void beginTransaction() throws PersistException;
    public abstract void commit() throws PersistException;
    public abstract void rollback() throws PersistException;
    public abstract void closeConnection() throws PersistException;
    @SuppressWarnings("rawtypes")
	public GenericDao getDao(Class dtoClass) throws PersistException;
}
