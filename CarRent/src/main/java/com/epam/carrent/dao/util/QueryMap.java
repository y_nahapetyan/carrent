package com.epam.carrent.dao.util;

import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

public class QueryMap {
	
	private String query;
	private Logger log = Logger.getRootLogger();
	
	public QueryMap(String sql){
		this.query = sql;
	}

	private String mapToQuery(Set<String> parameters){
		StringBuilder queryString = new StringBuilder();
		for(String entry : parameters){
			if(queryString.length() > 0)
				queryString.append(" and ");
			queryString.append(entry +" = ?");
		}
		String sql = queryString.toString();
		log.info(sql);
		return sql;
	}
	public <V> String getWhereClause(Map<String,V> keys){
		query += " where " + mapToQuery(keys.keySet());
		return query;
	}
}
