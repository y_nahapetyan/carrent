package com.epam.carrent.dao.connpool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

class ConnectionPoolConfig {
    private static final ResourceBundle bundle = ResourceBundle.getBundle("dao");
    private String driverClassName;
    private String url;
    private String user;
    private String password;

    public ConnectionPoolConfig() {
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Connection getConnection() throws ConnectionPoolException {
        if (driverClassName == null) driverClassName = bundle.getString("driver");
        if (url == null) url = bundle.getString("url");
        if (user == null) user = bundle.getString("user");
        if (password == null) password = bundle.getString("password");
        try {
            Class.forName(driverClassName);
        } catch (ClassNotFoundException e) {
            throw new ConnectionPoolException("driver not found", e);
        }
        try {
			return DriverManager.getConnection(url, user, password);
		} catch (SQLException e) {
			throw new ConnectionPoolException("can`t get connection", e);
		}
    }
}

