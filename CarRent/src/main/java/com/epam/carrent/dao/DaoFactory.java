package com.epam.carrent.dao;

import com.epam.carrent.dao.impl.DaoFactoryException;

public interface DaoFactory<Context> {

	public interface DaoCreator<Context>{
		@SuppressWarnings("rawtypes")
		public GenericDao create(Context context);
	}

    public abstract void releaseConnections() throws DaoFactoryException;
    public abstract DaoManager getDaoManager() throws DaoFactoryException;
}
