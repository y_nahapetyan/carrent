package com.epam.carrent.dao;

import java.util.List;

import com.epam.carrent.entity.Identifiable;

public interface GenericDao<T extends Identifiable> {
	public T create() throws PersistException;
	public T persist(T object) throws PersistException;
	public List<T> getAll() throws PersistException;
	public void delete(T object) throws PersistException;
	public void update(T object) throws PersistException;
	T getByPK(Integer key) throws PersistException;
}
