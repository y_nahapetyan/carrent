package com.epam.carrent.dao.impl;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.epam.carrent.dao.AbstractJDBCDao;
import com.epam.carrent.dao.PersistException;
import com.epam.carrent.dao.util.PasswdHashUtil;
import com.epam.carrent.entity.PageFilter;
import com.epam.carrent.entity.Role;
import com.epam.carrent.entity.User;

public class UserDao extends AbstractJDBCDao<User> {
	private final static int ITERATION_NUMBER = 1000;	
	Logger log = Logger.getRootLogger();
	public UserDao(Connection connection) {
		super(connection);
	}

	private class PersistUser extends User {
		public void setId(Integer id){
			super.setId(id);
		}
	}

	@Override
	public String getPKColumn() {
		return "id";
	}
	
	@Override
	public User create() throws PersistException {
		User user = new User();
		return persist(user);
	}

	@Override
	public String getSelectQuery() {
		return "select id, role_id, first_name, last_name, email, "
				+"passport, password, salt from carrent.users";
	}

	@Override
	public String getCreateQuery() {
		return "insert into carrent.users (role_id, first_name, last_name, email, passport, password, salt) "
				+"values (?, ?, ?, ?, ?, ?, ?);";
	}

	@Override
	public String getUpdateQuery() {	
		return "update carrent.users /n" +
				"set role_id = ?, first_name = ?, last_name = ?, email = ?, passport = ?, password = ?, salt = ? /n"
				+"where id = ?;";
	}

	@Override
	public String getDeleteQuery() {
		return "delete from carrent.users where id = ?;";
	}
	

	public User getByCredentials(String email, String password) throws PersistException{
		Map<String, String> map = new HashMap<String, String>();
		map.put("email", email);
		User user = super.getByQuery(getSelectQuery(), map);
		String digest, salt;
		boolean passwdMatches = false;
		
		if(user != null){
        digest = user.getPassword();
        salt = user.getSalt();
        log.info(digest);
        log.info(salt);
		    if (digest == null || salt == null) {
		        throw new PersistException("Database inconsistant Salt or Digested Password altered");
		    }
		} else {// TIME RESISTANT ATTACK (Even if the user does not exist the
            // Computation time is equal to the time needed for a legitimate user
            digest = "000000000000000000000000000=";
            salt = "00000000000=";		
		}
		
        try {
			byte[] bDigest = PasswdHashUtil.base64ToByte(digest);
			byte[] bSalt = PasswdHashUtil.base64ToByte(salt);
			// Compute the new DIGEST
			byte[] proposedDigest = PasswdHashUtil.getHash(ITERATION_NUMBER, password, bSalt);
			passwdMatches = Arrays.equals(proposedDigest, bDigest);
			log.info(passwdMatches);
		} catch (IOException | NoSuchAlgorithmException e) {
			throw new PersistException("Database inconsistant Salt or Digested Password altered");
		}
        
		return passwdMatches ? user : null;
	}
	
	public User getByEmail(String email) throws PersistException{
		Map<String, String> map = new HashMap<String, String>();
		map.put("email", email);
		return super.getByQuery(getSelectQuery(), map);
	}

	@Override
	protected List<User> parseResultSet(ResultSet rs) throws PersistException {
		List<User> result = new LinkedList<User>();
		try{
			while(rs.next()){				
				PersistUser user = new PersistUser();
				user.setId(rs.getInt("id"));
				Long id = rs.getLong("role_id");
				log.info("role id (long) = " + id);
				log.info("role id (int) = " + id.intValue());
				user.setRole(Role.values()[id.intValue()]);
				user.setEmail(rs.getString("email"));
				user.setPassport(rs.getString("passport"));
				user.setPassword(rs.getString("password"));
				user.setSalt(rs.getString("salt"));
				user.setFirstname(rs.getString("first_name"));
				user.setLastname(rs.getString("last_name"));
				result.add(user);
			}
		}
		catch(Exception e){
			throw new PersistException(e);
		}
		return result;
	}

	@Override
	protected void prepareStatementForInsert(PreparedStatement statement,
			User user) throws PersistException {
		String email = user.getEmail();
		String password = user.getPassword();
		if(email!= null && password!=null && email.length() <= 45){
			try{
			 // Uses a secure Random not a simple Random
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
            // Salt generation 64 bits long
            byte[] bSalt = new byte[8];
            random.nextBytes(bSalt);
            // Digest computation
            byte[] bDigest = PasswdHashUtil.getHash(ITERATION_NUMBER, password ,bSalt);
            String sDigest = PasswdHashUtil.byteToBase64(bDigest);
            String sSalt = PasswdHashUtil.byteToBase64(bSalt);
            log.info("role id (long) = " + user.getRole().ordinal());
				statement.setObject(1, user.getRole().ordinal());
				statement.setString(2, user.getFirstname());
				statement.setString(3, user.getLastname());
				statement.setString(4, user.getEmail());
				statement.setString(5, user.getPassport());
				statement.setString(6, sDigest);
				statement.setString(7, sSalt);
			}
			catch(Exception e){
				throw new PersistException(e);
			}
		}
	}

	@Override
	protected void prepareStatementForUpdate(PreparedStatement statement,
			User user) throws PersistException {
		String email = user.getEmail();
		String password = user.getPassword();
		if(email!= null && password!=null && email.length() <= 45){
			try{
			 // Uses a secure Random not a simple Random
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
            // Salt generation 64 bits long
            byte[] bSalt = new byte[8];
            random.nextBytes(bSalt);
            // Digest computation
            byte[] bDigest = PasswdHashUtil.getHash(ITERATION_NUMBER, password ,bSalt);
            String sDigest = PasswdHashUtil.byteToBase64(bDigest);
            String sSalt = PasswdHashUtil.byteToBase64(bSalt);

				statement.setObject(1, user.getRole().ordinal());
				statement.setString(2, user.getFirstname());
				statement.setString(3, user.getLastname());
				statement.setString(4, user.getEmail());
				statement.setString(5, user.getPassport());
				statement.setString(6, sDigest);
				statement.setString(7, sSalt);
				statement.setInt(8, user.getId());
			}
			catch(Exception e){
				throw new PersistException(e);
			}	
		}
	}

	@Override
	public String getQuantityQuery(StringBuilder preparedStatementBuilder) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getResultQuery(StringBuilder preparedStatementBuilder,
			PageFilter filter) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void prepareStatementForQtyQuery(PageFilter filter,
			PreparedStatement statement) throws PersistException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void prepareStatementForSearchResultQuery(int pageNumber,
			int pageLimit, PageFilter filter, PreparedStatement statement)
			throws PersistException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getExistQuery() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void prepareStatementForExistQuery(PreparedStatement statement,
			User object) throws PersistException {
		// TODO Auto-generated method stub
		
	}

}
