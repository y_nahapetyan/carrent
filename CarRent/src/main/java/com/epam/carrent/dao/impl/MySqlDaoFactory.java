package com.epam.carrent.dao.impl;

import java.sql.Connection;

import com.epam.carrent.dao.DaoFactory;
import com.epam.carrent.dao.DaoManager;
import com.epam.carrent.dao.connpool.ConnectionPool;
import com.epam.carrent.dao.connpool.ConnectionPoolException;

public class MySqlDaoFactory implements DaoFactory<Connection>{
    private ConnectionPool pool = null;
    private MySqlDaoManager daoManager = null;

	private static class Holder {
		private static final MySqlDaoFactory instance
			 = new MySqlDaoFactory();
	}
	
	public static MySqlDaoFactory getInstance(){
		return Holder.instance;
	}
	
	@Override
    public void releaseConnections() throws DaoFactoryException {
        try {
			pool.closeConnections();
		} catch (ConnectionPoolException e) {
			throw new DaoFactoryException(e);
		}
    }

	@Override
	public DaoManager getDaoManager() throws DaoFactoryException {
	    Connection connection = null;
        try {
			connection = pool.getConnection();
		} catch (ConnectionPoolException e) {
			throw new DaoFactoryException(e);
		}

	   daoManager = new MySqlDaoManager(connection);
	   return (DaoManager)daoManager;
	}
	
	private MySqlDaoFactory(){
		try {
			initConnections();
		} catch (DaoFactoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	
	private void initConnections() throws DaoFactoryException{
        pool = new ConnectionPool(10);
        try {
            pool.init();
        } catch (ConnectionPoolException e) {
        	throw new DaoFactoryException(e);
		}
	}
		
}
