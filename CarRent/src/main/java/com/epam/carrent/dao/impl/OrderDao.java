package com.epam.carrent.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.epam.carrent.dao.AbstractJDBCDao;
import com.epam.carrent.dao.PersistException;
import com.epam.carrent.entity.Car;
import com.epam.carrent.entity.Order;
import com.epam.carrent.entity.PageFilter;
import com.epam.carrent.entity.User;

public class OrderDao extends AbstractJDBCDao<Order>{

	public OrderDao(Connection connection) {
		super(connection);
	}
	
	private class PersistOrder extends Order {
		public void setId(Integer id){
			super.setId(id);
		}
	}

	@Override
	public Order create() throws PersistException {
		Order order = new Order();
		return persist(order);
	}

	@Override
	public String getPKColumn() {
		return "orders.id";
	}

	@Override
	public String getSelectQuery() {
		return "select orders.id,\n" +
                "orders.car_id,\n" +
                "orders.user_id,\n" +
                "orders.date_start,\n" +
                "orders.date_end,\n" +
                "orders.reason,\n" +
                "orders.penalty,\n" +
                "orders.rent_total,\n" +
                "models.model_name, \n" +
                "brands.brand_name, \n" +
                "orders.status \n" +
            "from carrent.orders join cars on cars.id = orders.car_id \n" +
            "join carrent.models on cars.car_model_id = models.id \n" +
            "join carrent.brands on models.brand_id = brands.id";
	}

	@Override
	public String getCreateQuery() {
		return "insert into carrent.orders \n" +
                  "(car_id, user_id, date_start, date_end, status, rent_total) \n" +
                  "values (?, ?, ?, ?, 'new', ?);";
	}

	@Override
	public String getUpdateQuery() {
		return "update carrent.orders \n" +
				"set car_id = ?, user_id = ?, date_start = ?, date_end = ?, status = ?, rent_total = ? \n" +
				"where id = ?;";
	}

	@Override
	public String getDeleteQuery() {
		return "delete from carrent.orders where id = ?;";
	}
	
	public String getExistQuery(){
		return "select orders.car_id from cars \n"+
				"join orders on orders.car_id = cars.id \n"+
				"where orders.status <> 'rejected' and (cars.id = ? \n"+
				"and ((date_start <= ? and  date_end>= ?) or \n"+
				"(date_start>= ? AND date_start <= ?)))";
	}

	public List<Order> getByUserIdAndStatus(Integer userId, String status) throws PersistException{
		Map<String, String> map = new HashMap<String, String>();
		map.put("orders.user_id", userId.toString());
		map.put("orders.status", status);
		return super.getManyByQuery(getSelectQuery(), map);
	}
	
	public List<Order> getAllOrders(String status) throws PersistException{
		Map<String, String> map = new HashMap<String, String>();
		map.put("orders.status", status);
		return super.getManyByQuery(getSelectQuery(), map);
	}

	@Override
	protected List<Order> parseResultSet(ResultSet rs) throws PersistException {
		List<Order> result = new LinkedList<Order>();
		try{
			while(rs.next()){				
				PersistOrder order = new PersistOrder();
				order.setCar(new Car());
				order.setUser(new User());
		        order.setId(rs.getInt("id"));
		        order.setBrand(rs.getString("brand_name"));
		        order.setModel(rs.getString("model_name"));
		        order.setRentTotal(rs.getDouble("rent_total"));
		        order.setPenalty(rs.getDouble("penalty"));
		        order.setReason(rs.getString("reason"));
		        order.setStatus(rs.getString("status"));
		        order.setDateStart(rs.getDate("date_start"));
		        order.setDateEnd(rs.getDate("date_end"));
		        order.getCar().setId(rs.getInt("car_id"));
		        order.getUser().setId(rs.getInt("user_id"));
				result.add(order);
			}
		}
		catch(Exception e){
			throw new PersistException(e);
		}
		return result;
	}

	@Override
	protected void prepareStatementForInsert(PreparedStatement statement,
			Order order) throws PersistException {
		//return "insert into carrent.orders \n" +
               // "(car_id, user_id, date_start, date_end, status, rent_total) \n" +
               // "values (?, ?, ?, ?, 'new', ?);";
		try {
				statement.setInt(1, order.getCar().getId());
				statement.setInt(2, order.getUser().getId());
				statement.setDate(3, order.getDateStart());
				statement.setDate(4, order.getDateEnd());
				statement.setDouble(5, order.getRentTotal());
			} catch(Exception e){
				throw new PersistException(e);
		}			
	}

	@Override
	protected void prepareStatementForUpdate(PreparedStatement statement,
			Order order) throws PersistException {
		try {
				statement.setInt(1, order.getCar().getId());
				statement.setInt(2, order.getUser().getId());
				statement.setDate(3, order.getDateStart());
				statement.setDate(4, order.getDateEnd());
				statement.setString(5, order.getStatus());
				statement.setDouble(6, order.getRentTotal());
				statement.setInt(7, order.getId());
		} catch(Exception e){
			throw new PersistException(e);
		}
	}
	
	@Override
	protected void prepareStatementForExistQuery(PreparedStatement statement,
			Order order) throws PersistException {
		try {
			statement.setInt(1, order.getCar().getId());
			statement.setDate(2, order.getDateStart());
			statement.setDate(3, order.getDateStart());
			statement.setDate(4, order.getDateStart());
			statement.setDate(5, order.getDateEnd());
		} catch(Exception e){
			throw new PersistException(e);
		}
	}

	@Override
	public String getQuantityQuery(StringBuilder preparedStatementBuilder) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getResultQuery(StringBuilder preparedStatementBuilder,
			PageFilter filter) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void prepareStatementForQtyQuery(PageFilter filter,
			PreparedStatement statement) throws PersistException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void prepareStatementForSearchResultQuery(int pageNumber,
			int pageLimit, PageFilter filter, PreparedStatement statement)
			throws PersistException {
		// TODO Auto-generated method stub
		
	}

}
