package com.epam.carrent.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.epam.carrent.dao.util.QueryMap;
import com.epam.carrent.entity.Identifiable;
import com.epam.carrent.entity.PageFilter;

public abstract class AbstractJDBCDao<T extends Identifiable> implements GenericDao<T>{
	protected Connection connection;
	
	public abstract String getPKColumn();
	public abstract String getSelectQuery();
	public abstract String getCreateQuery();
	public abstract String getUpdateQuery();
	public abstract String getDeleteQuery();
	
	//
	public abstract String getQuantityQuery(StringBuilder preparedStatementBuilder);
	public abstract String getResultQuery(StringBuilder preparedStatementBuilder, PageFilter filter);
	public abstract String getExistQuery();
	//
	protected abstract List<T> parseResultSet(ResultSet rs) throws PersistException;
	protected abstract void prepareStatementForInsert(PreparedStatement statement, T object) throws PersistException;
	protected abstract void prepareStatementForUpdate(PreparedStatement statement, T object) throws PersistException;
	protected abstract void prepareStatementForExistQuery(PreparedStatement statement,
			T object) throws PersistException;
	//
	protected abstract void prepareStatementForQtyQuery(PageFilter filter, PreparedStatement statement) throws PersistException;
	protected abstract void prepareStatementForSearchResultQuery(int pageNumber, int pageLimit,
			PageFilter filter, PreparedStatement statement) throws PersistException;
	
	public AbstractJDBCDao(Connection connection){
		this.connection = connection;
	}
	
	@Override
	public T persist(T object) throws PersistException {
		T persistInstance;
		String sql = getCreateQuery();

		try (PreparedStatement statement = 
				connection.prepareStatement(sql)){
			prepareStatementForInsert(statement, object);
			int count = statement.executeUpdate();

			if (count != 1) {
				throw new PersistException
				("on persist more than 1 record modified" + count);
			}
		} catch (SQLException e) {
			throw new PersistException(e);
		}
		
	sql = getSelectQuery() + " where "+getPKColumn()+" = last_insert_id()";
	System.out.println(sql);
		
	try (PreparedStatement statement = connection.prepareStatement(sql)){
	ResultSet rs = statement.executeQuery();
	List<T> list = parseResultSet(rs);
		if((list == null) || (list.size() != 1)){
				throw new PersistException("");
			}
		persistInstance = list.iterator().next();
	} catch (SQLException e) {
			throw new PersistException(e);
		}
		return persistInstance;
	}
	
	@Override
	public void update(T object) throws PersistException{
		String sql = getUpdateQuery();
		try(PreparedStatement statement = connection.prepareStatement(sql)){
			prepareStatementForUpdate(statement, object);
			int count = statement.executeUpdate();
			if(count != 1){
				throw new PersistException("more than 1 record modified");
			}
		}
		catch(Exception e){
			throw new PersistException(e);
		}
	}
	
	@Override
	public T getByPK(Integer key) throws PersistException {
		List<T> list;
		String sql = getSelectQuery();
		sql += " where "+ getPKColumn() +" = ?";
		try (PreparedStatement statement = connection.prepareStatement(sql)){
			statement.setLong(1, key);
			ResultSet rs = statement.executeQuery();
			list = parseResultSet(rs);
		} catch (SQLException e) {
			throw new PersistException(e);
		}
		if((list == null) || (list.size() == 0)){
			throw new PersistException("");
		}
		if(list.size() > 1){
			throw new PersistException("");
		}
		return list.iterator().next();
	}
	
	public T getByQuery(String query, Map<String, String> params) throws PersistException {
		List<T> list;
		String sql = new QueryMap(query).getWhereClause(params);

		try (PreparedStatement statement = connection.prepareStatement(sql)){
			int index = 1;
			for(String key : params.values()){
				//log.info(index + " - "+key);
				statement.setObject(index++, key);
			}
			//log.info(statement.toString());
			ResultSet rs = statement.executeQuery();
			list = parseResultSet(rs);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistException(e);
		}
		if((list == null) || (list.size() == 0)){
			return null;
		}

		return  list.iterator().next();
	}
	
	public List<T> getManyByQuery(String query, Map<String, String> params) throws PersistException {
		List<T> list;
		String sql = new QueryMap(query).getWhereClause(params);
		System.out.println(sql);
		try (PreparedStatement statement = connection.prepareStatement(sql)){
			int index = 1;
			for(String key : params.values()){
				System.out.println(index + " - "+key);
				statement.setObject(index++, key);
			}
			ResultSet rs = statement.executeQuery();
			list = parseResultSet(rs);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistException(e);
		}
		if((list == null) || (list.size() == 0)){
			throw new PersistException("");
		}

		return list;
	}
	
	@Override
	public void delete(T object) throws PersistException{
		String sql = getDeleteQuery();
		try (PreparedStatement statement = connection.prepareStatement(sql)){
			try {
				statement.setObject(1, object.getId());
			}
			catch(Exception e){
				throw new PersistException(e);
			}
			int count = statement.executeUpdate();
			if(count != 1){
				throw new PersistException("More than 1 record modified");
			}
			statement.close();
		}
		catch(Exception e){
			throw new PersistException(e);
		}
	}
	
	@Override
	public List<T> getAll() throws PersistException{
		List<T> list;
		String sql = getSelectQuery();
		try (PreparedStatement statement = connection.prepareStatement(sql)){
			ResultSet rs = statement.executeQuery();
			list = parseResultSet(rs);
		} catch (SQLException e) {
			throw new PersistException(e);
		}
		return list;	
	}
	
	public boolean exist(T object) throws PersistException{
		String sql = getExistQuery();
		try(PreparedStatement statement = connection.prepareStatement(sql)){
			prepareStatementForExistQuery(statement, object);
			ResultSet rs = statement.executeQuery();
			return rs.next();
		}
		catch(Exception e){
			throw new PersistException(e);
		}
	}
	
	public List<T> getFilteredPage(int pageNumber, int pageLimit, PageFilter filter) throws PersistException{
		List<T> list;
        StringBuilder preparedStatementBuilder = new StringBuilder();
		String result = getResultQuery(preparedStatementBuilder, filter);
		String quantity = getQuantityQuery(preparedStatementBuilder);
		try (PreparedStatement statement = connection.prepareStatement(result)){
			prepareStatementForSearchResultQuery(pageNumber, pageLimit,
					filter, statement);
			//log.info(statement);
			ResultSet rs = statement.executeQuery();
			list = parseResultSet(rs);
		} catch (SQLException e) {
			throw new PersistException(e);
		}
		
		try (PreparedStatement statement = connection.prepareStatement(quantity)){
			prepareStatementForQtyQuery(filter, statement);
			ResultSet rs = statement.executeQuery();
	        if (rs.next()){
	            filter.setResultQty(rs.getInt(1));
	        }
		} catch (SQLException e) {
			throw new PersistException(e);
		}
		return list;	
	}

}
