package com.epam.carrent.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.epam.carrent.dao.AbstractJDBCDao;
import com.epam.carrent.dao.PersistException;
import com.epam.carrent.dao.impl.util.DateFilterStatus;
import com.epam.carrent.entity.Car;
import com.epam.carrent.entity.CarFilter;
import com.epam.carrent.entity.PageFilter;
import com.epam.carrent.dao.impl.util.CarFilterQueryBuilder;

public class CarDao extends AbstractJDBCDao<Car>{
	
	public CarDao(Connection connection) {
		super(connection);
	}
	
	private class PersistCar extends Car {
		public void setId(Integer id){
			super.setId(id);
		}
	}

	@Override
	public Car create() throws PersistException {
		Car car = new Car();
		return persist(car);
	}

	@Override
	public String getPKColumn() {
		return "cars.id";
	}

	@Override
	public String getSelectQuery() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getCreateQuery() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getUpdateQuery() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDeleteQuery() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String getFilterQuery(){
		return "select cars.id,\n" +
                "models.model_name,\n" +
                "brands.brand_name,\n"+
                "classes.class_name,\n"+
                "cars.price,\n"+
                "cars.is_automat,\n"+
                "cars.is_diesel,\n"+
                "cars.has_condition,\n"+
                "cars.door_qty \n"+
            "from cars join models on cars.car_model_id = models.id \n"+
            "join brands on models.brand_id = brands.id \n"+
            "join classes on models.car_class_id = classes.id";
	}
	
	public String getCountQuery(){
		return "select count(*) \n"+
            "from cars join models on cars.car_model_id = models.id \n" +
            "join brands on models.brand_id = brands.id \n"+
            "join classes on models.car_class_id = classes.id";
	}
	
	public String getRequestLimit(){
		return " order by cars.id limit ?, ?";
	}
	
  private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
  private String endDateSubstituteString = CarFilterQueryBuilder.createEndDateSubstitute(simpleDateFormat);
  private String startDateSubstituteString = simpleDateFormat.format(new Date());
  private DateFilterStatus dateFilterStatus = DateFilterStatus.BOTH_NULL;
  
	@Override
	public String getQuantityQuery(StringBuilder preparedStatementBuilder) {
        String qtyRequest = getCountQuery() + preparedStatementBuilder.toString();
		return qtyRequest;
	}

	@Override
	public String getResultQuery(StringBuilder preparedStatementBuilder, PageFilter carFilter) {
        dateFilterStatus = 
        		CarFilterQueryBuilder.createFilterCriteria((CarFilter)carFilter, preparedStatementBuilder);       
        String resultRequest = getFilterQuery() + preparedStatementBuilder.toString();
		return resultRequest + "" + getRequestLimit();
	}
	
	@Override
	protected List<Car> parseResultSet(ResultSet rs) throws PersistException {
		List<Car> result = new LinkedList<Car>();
		try{
			while(rs.next()){				
				PersistCar car = new PersistCar();
		        car.setId(rs.getInt("id"));
	            car.setModel(rs.getString("model_name"));
	            car.setBrand(rs.getString("brand_name"));
	            car.setClassName(rs.getString("class_name"));
	            car.setPrice(rs.getDouble("price"));
	            car.setIsAutomat(rs.getBoolean("is_automat"));
	            car.setIsDiesel(rs.getBoolean("is_diesel"));
	            car.setHasCondition(rs.getBoolean("has_condition"));
	            car.setDoorQty(rs.getInt("door_qty"));
				result.add(car);
			}
		}
		catch(Exception e){
			throw new PersistException(e);
		}
		return result;
	}


	protected void prepareStatementForReserveQuery(PreparedStatement statement,
			int userId, Car car, String beginDate, String endDate, int dayQty) throws PersistException {
		try {
	        statement.setInt(1, car.getId());
	        statement.setInt(2, userId);
	        statement.setString(3, beginDate);
	        statement.setString(4, endDate);
	        statement.setDouble(5, dayQty * car.getPrice());
			} catch(Exception e){
				throw new PersistException(e);
		}			
	}
	@Override
	protected void prepareStatementForQtyQuery(PageFilter carFilter, 
			PreparedStatement statement) throws PersistException {
		try {
			String beginDate = ((CarFilter)carFilter).getBeginDate();
			String endDate = ((CarFilter)carFilter).getEndDate();
	       switch (dateFilterStatus){
            case BEGIN_END_SET:
                statement.setString(1, beginDate);
                statement.setString(2, beginDate);
                statement.setString(3, beginDate);
                statement.setString(4, endDate);
                break;
            case BEGIN_SET:
                statement.setString(1, beginDate);
                statement.setString(2, beginDate);
                statement.setString(3, beginDate);
                statement.setString(4, endDateSubstituteString);
                break;
            case END_SET:
                statement.setString(1, startDateSubstituteString);
                statement.setString(2, endDate);
                break;
	       }
		} catch(Exception e){
			throw new PersistException(e);
		}
	}
	
	@Override
	protected void prepareStatementForSearchResultQuery(int pageNumber, int pageLimit,
			PageFilter carFilter, PreparedStatement statement) throws PersistException {
		try {
			String beginDate = ((CarFilter)carFilter).getBeginDate();
			String endDate = ((CarFilter)carFilter).getEndDate();
	        switch (dateFilterStatus){
            case BEGIN_END_SET:
                statement.setString(1, beginDate);
                statement.setString(2, beginDate);
                statement.setString(3, beginDate);
                statement.setString(4, endDate);
                statement.setInt(5, (pageNumber - 1) * pageLimit);
                statement.setInt(6, pageLimit);
                break;
            case BEGIN_SET:
                statement.setString(1, beginDate);
                statement.setString(2, beginDate);
                statement.setString(3, beginDate);
                statement.setString(4, endDateSubstituteString);
                statement.setInt(5, (pageNumber - 1) * pageLimit);
                statement.setInt(6, pageLimit);
                break;
            case END_SET:
                statement.setString(1, startDateSubstituteString);
                statement.setString(2, endDate);
                statement.setInt(3, (pageNumber - 1) * pageLimit);
                statement.setInt(4, pageLimit);
                break;
            default:
                statement.setInt(1, (pageNumber - 1) * pageLimit);
                statement.setInt(2, pageLimit);
        }
		} catch(Exception e){
			throw new PersistException(e);
	}		
		
}

	@Override
	protected void prepareStatementForInsert(PreparedStatement statement,
			Car object) throws PersistException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void prepareStatementForUpdate(PreparedStatement statement,
			Car object) throws PersistException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getExistQuery() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void prepareStatementForExistQuery(PreparedStatement statement,
			Car object) throws PersistException {
		// TODO Auto-generated method stub
		
	}

}
