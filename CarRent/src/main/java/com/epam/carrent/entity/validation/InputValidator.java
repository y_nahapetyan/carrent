package com.epam.carrent.entity.validation;

public class InputValidator {
	private static final String NAME_PATTERN = "[A-Za-zА-Яа-я]+";
	private static final String PASSPORT_PATTERN = "[A-Za-zА-Яа-я0-9]+";
	private static final String EMAIL_PATTERN = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@" +
            "(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
	private static final int DEFAULT_FIELD_LENGTH = 50;

    public static boolean isValidName(String name){
    	return name.matches(NAME_PATTERN);
    }
    
    public static boolean isValidPassport(String passport){
    	return passport.matches(PASSPORT_PATTERN);
    }
    
    public static boolean isValidEmail(String email){
    	return email.matches(EMAIL_PATTERN);
    }
    
    public static boolean isValidLength(String field){
    	return field.length() < DEFAULT_FIELD_LENGTH;
    }
    
}
