package com.epam.carrent.entity;

public interface PageFilter {
    public int getResultQty();
    public void setResultQty(int resultQty);
}
