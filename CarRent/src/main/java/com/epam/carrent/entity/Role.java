package com.epam.carrent.entity;

import org.apache.log4j.Logger;

public enum Role {
	CLIENT("CLIENT"),
	ADMIN("ADMIN"),
	UNAUTORIZED("UNAUTORIZED");
	
    private static final Logger logger = Logger.getLogger(Role.class);
	private String name;

	Role(String name){
		this.name = name;
	}
	
   public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.getName();
    }

    public static Role getEnum(String name) {
        for(Role n : values()){
            if(n.getName().equalsIgnoreCase(name)) 
            	logger.info("def role = "+name);
            	return n;
            }
    	logger.info("def role = "+name);
        return Role.UNAUTORIZED;
    }
}
