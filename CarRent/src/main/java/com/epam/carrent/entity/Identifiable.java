package com.epam.carrent.entity;

public interface Identifiable {
	public Integer getId();
}
