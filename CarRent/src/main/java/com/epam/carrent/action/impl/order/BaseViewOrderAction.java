package com.epam.carrent.action.impl.order;

import com.epam.carrent.action.ActionException;
import com.epam.carrent.action.BaseAction;
import com.epam.carrent.dao.PersistException;
import com.epam.carrent.dao.impl.MySqlDaoFactory;
import com.epam.carrent.dao.impl.MySqlDaoManager;
import com.epam.carrent.dao.impl.OrderDao;
import com.epam.carrent.entity.Order;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

public class BaseViewOrderAction extends BaseAction {

    private static final Logger log = Logger.getLogger(BaseViewOrderAction.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ActionException {
        if (isAccessNotPermitted(request, response)) return;
        
		MySqlDaoManager daoManager = null;
        try {
        	daoManager = (MySqlDaoManager) MySqlDaoFactory.getInstance().getDaoManager();
            int orderId = Integer.parseInt(request.getParameter("id"));
            OrderDao orderDao = (OrderDao) daoManager.getDao(Order.class);
            Order orderChosen = orderDao.getByPK(orderId);
            if (orderChosen.getBrand() == null) {
                redirectToMain(request, response, "ORDER_BY_ID_REQUEST_FAILED" );
            } else {
                request.getSession().setAttribute("order", orderChosen);
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/order" + ".tiles");
                requestDispatcher.forward(request, response);
            }
        } catch (NumberFormatException e) {
            redirectToMain(request, response, "ORDER_BY_ID_REQUEST_FAILED" );
        } catch (PersistException e) {
            //redirectToMain(request, response, "DATABASE_PROBLEM" );
            log.error("DBError", e);
            throw new ActionException("DB exception", e);
        } catch (Exception e) {
            log.error(e);
            throw new ActionException(e);
        } finally {
        	try {
				daoManager.closeConnection();
			} catch (PersistException e) {
	            throw new ActionException("DB exception", e);
			}
        }
    }


}

