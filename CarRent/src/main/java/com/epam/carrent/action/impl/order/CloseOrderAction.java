package com.epam.carrent.action.impl.order;

import com.epam.carrent.action.Action;
import com.epam.carrent.action.ActionException;
import com.epam.carrent.dao.PersistException;
import com.epam.carrent.dao.impl.MySqlDaoFactory;
import com.epam.carrent.dao.impl.MySqlDaoManager;
import com.epam.carrent.dao.impl.OrderDao;
import com.epam.carrent.entity.Order;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

public class CloseOrderAction implements Action {

    private static final Logger log = Logger.getLogger(CloseOrderAction.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ActionException {
		MySqlDaoManager daoManager = null;
        try {
        	daoManager = (MySqlDaoManager) MySqlDaoFactory.getInstance().getDaoManager();
            request.getSession().removeAttribute("error");
            int orderId = Integer.parseInt(request.getParameter("id"));
            
            OrderDao orderDao = (OrderDao) daoManager.getDao(Order.class);
            Order orderChosen = orderDao.getByPK(orderId);
            orderChosen.setStatus("closed");
            orderDao.update(orderChosen);

            request.getSession().setAttribute("order", orderDao.getByPK(orderId));
            
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/order" +".tiles");
            requestDispatcher.forward(request, response);

        } catch (NumberFormatException e) {
            request.getSession().setAttribute("error", "ORDER_BY_ID_REQUEST_FAILED");
        } catch (PersistException e) {
            //request.getSession().setAttribute("error", "DATABASE_PROBLEM");
            log.error("DB", e);
            throw new ActionException("DB exception", e);
        } catch (Exception e) {
            throw new ActionException(e);
        } finally {
        	try {
				daoManager.closeConnection();
			} catch (PersistException e) {
	            throw new ActionException("persist exception at conn. close", e);
			}
        }
    }
}

