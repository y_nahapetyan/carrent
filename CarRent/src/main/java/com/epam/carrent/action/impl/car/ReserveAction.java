package com.epam.carrent.action.impl.car;

import com.epam.carrent.action.ActionException;
import com.epam.carrent.action.BaseAction;
import com.epam.carrent.dao.PersistException;
import com.epam.carrent.dao.impl.DaoFactoryException;
import com.epam.carrent.dao.impl.MySqlDaoFactory;
import com.epam.carrent.dao.impl.MySqlDaoManager;
import com.epam.carrent.dao.impl.OrderDao;
import com.epam.carrent.entity.Car;
import com.epam.carrent.entity.Order;
import com.epam.carrent.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ReserveAction extends BaseAction {

    private static final Logger log = Logger.getLogger(ReserveAction.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ActionException {
    	
        if (request.getMethod().toLowerCase().equals("get")){
            redirectToReservePageWithCarSetted(request, response);
            return;
        }
	
        Car car = (Car) request.getSession().getAttribute("car");
        User user = (User) request.getSession().getAttribute("user");
        String beginDateStr = request.getParameter("beginDate");
        String endDateStr = request.getParameter("endDate");
        int dayDifference = getDiffInDays(beginDateStr, endDateStr);
        if (dayDifference < 1){
            request.getSession().setAttribute("error", "INCORRECT_DATES");
            dispatcherForward(request, response, request.getRequestDispatcher("/reserve" +".tiles"));
            return;
        }
        Order order = new Order();
        order.setUser(user);
        order.setCar(car);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
			order.setDateStart(new java.sql.Date(dateFormat.parse(beginDateStr).getTime()));
	        order.setDateEnd(new java.sql.Date(dateFormat.parse(endDateStr).getTime()));
		} catch (ParseException e1) {
            throw new ActionException("date parse exception", e1);
		}

        order.setRentTotal(dayDifference * car.getPrice());
        MySqlDaoManager daoManager = null;
        try {
    		daoManager = (MySqlDaoManager) MySqlDaoFactory.getInstance().getDaoManager();
            OrderDao orderDao = (OrderDao) daoManager.getDao(Order.class);
            if (orderDao.exist(order)) {
                request.getSession().setAttribute("error", "CAR_NOT_AVAILABLE");
                dispatcherForward(request, response, request.getRequestDispatcher("/reserve" +".tiles"));
            } else {
            	orderDao.persist(order);
                redirectToMain(request, response, "RESERVE_SUCCESS" );
            }
            
        } catch (PersistException | DaoFactoryException e) {
           // request.getSession().setAttribute("error", "CAR_RESERVATION_FAIL_SQL");
            //dispatcherForward(request, response, request.getRequestDispatcher("/reserve" +".tiles"));
            log.error("DB error", e);
            throw new ActionException("DB error", e);
        } finally {
        	try {
				daoManager.closeConnection();
			} catch (PersistException e) {
                log.error("DB err", e);
                throw new ActionException("at connection close", e);
			}
        }
    }

    private void redirectToReservePageWithCarSetted(HttpServletRequest request, HttpServletResponse response) throws ActionException {
        request.getSession().removeAttribute("error");
        int carId = Integer.parseInt(request.getParameter("id"));
        Car carChosen = findChosenCar(request, carId);
        request.getSession().setAttribute("car", carChosen);
        dispatcherForward(request, response, request.getRequestDispatcher("/reserve" +".tiles"));
    }

    private Car findChosenCar(HttpServletRequest request, int carId) {
        @SuppressWarnings("unchecked")
		List<Car> cars = (List<Car>) request.getSession().getAttribute("cars");
        Car carChosen = null;
        for (Car car: cars){
            if (car.getId() == carId) {
                carChosen = car;
                break;
            }
        }
        return carChosen;
    }

    private int getDiffInDays(String beginDateStr, String endDateStr) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date beginDate = null;
        Date endDate = null;
        try {
            beginDate = dateFormat.parse(beginDateStr);
            endDate = dateFormat.parse(endDateStr);
        } catch (ParseException e) {
            log.error("date parse exception", e);
        }
        long timeDifference = endDate.getTime() - beginDate.getTime();
        return (int) (((timeDifference /1000) / 3600) / 24);
    }
}
