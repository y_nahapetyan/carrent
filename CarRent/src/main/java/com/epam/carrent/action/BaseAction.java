package com.epam.carrent.action;

import java.io.IOException;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;

import com.epam.carrent.entity.Role;
import com.epam.carrent.entity.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BaseAction implements Action {

    private static final Logger logger = Logger.getLogger(BaseAction.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ActionException {
        redirectToMain(request, response, "BAD_COMMAND" );
    }

    public void redirectToMain(HttpServletRequest request, HttpServletResponse response, String message) throws ActionException{
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/info.tiles");
        request.setAttribute("info", message);
        dispatcherForward(request, response, requestDispatcher);
    }

    public void dispatcherForward(HttpServletRequest request, HttpServletResponse response, RequestDispatcher requestDispatcher) throws ActionException {
        try {
            requestDispatcher.forward(request, response);
        } catch (Exception e) {
            logger.error("Forward", e);
			requestDispatcher = request.getRequestDispatcher("/404" + ".tiles");
            try {
				requestDispatcher.forward(request, response);
			} catch (ServletException | IOException e1) {
				throw new ActionException(e);
			} 
        }
    }

    public boolean isAccessNotPermitted(HttpServletRequest request, HttpServletResponse response) throws ActionException {
        User user = (User) request.getSession().getAttribute("user");
        if (user == null || !user.getRole().equals(Role.ADMIN) ) {
            redirectToMain(request, response, "LOG_IN_WARN");
            return true;
        }
        return false;
    }


    public User getUserFromParameters(HttpServletRequest request) {
        User user = new User();
        try {
            BeanUtils.populate(user, request.getParameterMap());
        } catch (ReflectiveOperationException e) {
            logger.error("BeanUtilsError", e);
        }
        return user;
    }

    public RequestDispatcher getSamePageDispatcher(HttpServletRequest request) {
        String[] path = request.getServletPath().split("/");
        if (path.length < 2)
            return request.getRequestDispatcher("/index" +".tiles");
        return request.getRequestDispatcher("/"+ path[1] +".tiles");
    }
}
