package com.epam.carrent.action.impl.car;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;

import com.epam.carrent.action.ActionException;
import com.epam.carrent.action.BaseAction;
import com.epam.carrent.dao.PersistException;
import com.epam.carrent.dao.impl.CarDao;
import com.epam.carrent.dao.impl.DaoFactoryException;
import com.epam.carrent.dao.impl.MySqlDaoFactory;
import com.epam.carrent.dao.impl.MySqlDaoManager;
import com.epam.carrent.entity.Car;
import com.epam.carrent.entity.CarFilter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.List;

public class SearchCarAction extends BaseAction {

    private static final Logger log = Logger.getLogger(SearchCarAction.class);
    public static final int PAGE_LIMIT = 7;

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ActionException {
        String[] path = request.getServletPath().split("/");
        Integer pageNumber = getPageNumber(path);
        CarFilter carFilter = getCarFilter(request);
        try {
            createCarPage(request, pageNumber, carFilter);
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/"+ path[1] +".tiles");
            requestDispatcher.forward(request, response);
        }  catch (ServletException | IOException e) {
        	throw new ActionException(e);
		} 
    }

    private void createCarPage(HttpServletRequest request, Integer pageNumber, CarFilter carFilter) throws ActionException {
		MySqlDaoManager daoManager = null;
        List<Car> cars = null;
        try {
        	daoManager = (MySqlDaoManager) MySqlDaoFactory.getInstance().getDaoManager();
            CarDao carDao = (CarDao) daoManager.getDao(Car.class);
            cars = carDao.getFilteredPage(pageNumber, PAGE_LIMIT, carFilter);           
        } catch (PersistException | DaoFactoryException  e){
            //request.getSession().setAttribute("error", "DATABASE_PROBLEM");
            log.error("DB error", e);
            throw new ActionException("persistence error..", e);
        }  finally {
        	try {
				daoManager.closeConnection();
			} catch (PersistException e) {
				throw new ActionException("at connection close", e);
			}
        }
        Integer carCount = carFilter.getResultQty();

        int totalPages = carCount/PAGE_LIMIT + 1;
        int current = pageNumber;
        int begin = Math.max(1, current - 2);
        int end = Math.min(begin + 5, totalPages);

        request.getSession().setAttribute("beginIndex", begin);
        request.getSession().setAttribute("endIndex", end);
        request.getSession().setAttribute("currentIndex", pageNumber);
        request.getSession().setAttribute("totalPages", totalPages);
        request.getSession().setAttribute("cars", cars);
        request.getSession().setAttribute("carFilter", carFilter);
    }

    private CarFilter getCarFilter(HttpServletRequest request) {
        CarFilter carFilter = new CarFilter();
        if (request.getMethod().toLowerCase().equals("post")) {
            try {
                BeanUtils.populate(carFilter, request.getParameterMap());
            } catch (ReflectiveOperationException e) {
                log.error("Bean util error", e);
            }
        } else {
            carFilter = (CarFilter) request.getSession().getAttribute("carFilter");
            if (carFilter == null) carFilter = new CarFilter();
        }
        return carFilter;
    }

    //:-((
    private Integer getPageNumber(String[] path) {
        Integer pageNumber = null;
        if (path.length > 3){
            pageNumber = Integer.parseInt(path[3]);
        }else {
            pageNumber = 1;
        }
        return pageNumber;
    }
}
