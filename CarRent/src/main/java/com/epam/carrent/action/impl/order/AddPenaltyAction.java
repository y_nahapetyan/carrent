package com.epam.carrent.action.impl.order;

import com.epam.carrent.action.ActionException;
import com.epam.carrent.action.BaseAction;
import com.epam.carrent.dao.PersistException;
import com.epam.carrent.dao.impl.DaoFactoryException;
import com.epam.carrent.dao.impl.MySqlDaoFactory;
import com.epam.carrent.dao.impl.MySqlDaoManager;
import com.epam.carrent.dao.impl.OrderDao;
import com.epam.carrent.entity.Order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

public class AddPenaltyAction extends BaseAction {

    private static final Logger log = Logger.getLogger(AddPenaltyAction.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ActionException {
		MySqlDaoManager daoManager = null;
    	try {
    		daoManager = (MySqlDaoManager) MySqlDaoFactory.getInstance().getDaoManager();
            request.getSession().removeAttribute("error");
            int orderId = Integer.parseInt(request.getParameter("id"));
            double penalty = Double.parseDouble(request.getParameter("penalty"));

            OrderDao orderDao = (OrderDao) daoManager.getDao(Order.class);
            Order orderChosen = orderDao.getByPK(orderId);
            orderChosen.setPenalty(penalty);
            orderDao.update(orderChosen);

            request.getSession().setAttribute("order", orderDao.getByPK(orderId));

        } catch (NumberFormatException e) {
            request.getSession().setAttribute("error", "ORDER_BY_ID_REQUEST_FAILED");
        }  catch (PersistException | DaoFactoryException e) {
            //request.getSession().setAttribute("error", "DATABASE_PROBLEM");
            log.error("DB error", e);
            throw new ActionException("DB exception", e);
        } finally {
        	try {
				daoManager.closeConnection();
			} catch (PersistException e) {
	            throw new ActionException("DB exception", e);
			}
        }

        dispatcherForward(request, response, request.getRequestDispatcher("/order" +".tiles") );

    }
}

