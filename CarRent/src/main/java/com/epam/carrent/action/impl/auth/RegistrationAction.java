package com.epam.carrent.action.impl.auth;

import com.epam.carrent.action.ActionException;
import com.epam.carrent.action.BaseAction;
import com.epam.carrent.dao.PersistException;
import com.epam.carrent.dao.impl.DaoFactoryException;
import com.epam.carrent.dao.impl.MySqlDaoFactory;
import com.epam.carrent.dao.impl.MySqlDaoManager;
import com.epam.carrent.dao.impl.UserDao;
import com.epam.carrent.entity.Role;
import com.epam.carrent.entity.User;
import com.epam.carrent.entity.validation.InputValidator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

public class RegistrationAction extends BaseAction {

    private static final Logger log = Logger.getLogger(RegistrationAction.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ActionException {	
        User user = getUserFromParameters(request);
        HttpSession session = request.getSession();
        clearErrors(session);

        boolean isUserValid = persistUser(session, user);
        session.setAttribute("user", user);

        if (!isUserValid) {
            dispatcherForward(request, response, request.getRequestDispatcher("/registration.tiles"));
        } else {
            redirectToMain(request, response, "REG_SUCCESS" );
        }
    }

    private boolean persistUser(HttpSession session, User user) throws ActionException {
        if (isValidUser(user, session)) {
        	MySqlDaoManager daoManager = null;
            try {
            	daoManager = (MySqlDaoManager) MySqlDaoFactory.getInstance().getDaoManager();
    			UserDao dao = (UserDao) daoManager.getDao(User.class);
    	        user.setRole(Role.CLIENT);
    			User created = dao.persist(user);
                return created != null ? true : false;
            } catch (PersistException | DaoFactoryException e) {
                session.setAttribute("emailError", "DOUBLE_EMAIL");
                log.warn("DB error", e);
                throw new ActionException("user persistence error", e);
			}  finally {
				try {
					daoManager.closeConnection();
				} catch (PersistException e) {
	                log.error("DB err", e);
	                throw new ActionException("at connection close", e);
				}
			}
        }
        return false;
    }

    private boolean isValidUser(User user, HttpSession session) {
        boolean isValid = true;
        if (!user.getPassword().equals(user.getRepassword()) || user.getPassword().isEmpty()){
            isValid = false;
            session.setAttribute("passwordError", "INCORRECT_PASSWORD");
        }else {
        	if (!InputValidator.isValidName(user.getFirstname())){
        		isValid = false;
        		session.setAttribute("firstnameError", "BLANK_WRONG_SYMBOLS");
        	}
        	if (!InputValidator.isValidLength(user.getFirstname())){
        		isValid = false;
        		 session.setAttribute("firstnameError", "BAD_LENGTH");
        	}
        	if(!InputValidator.isValidName(user.getLastname())){
        		isValid = false;
        		 session.setAttribute("lastnameError", "BLANK_WRONG_SYMBOLS");
        	}
        	if(!InputValidator.isValidLength(user.getLastname())){
        		isValid = false;
        		 session.setAttribute("lastnameError", "BAD_LENGTH");
        	}
        	if(!InputValidator.isValidPassport(user.getPassport())){
        		isValid = false;
        		session.setAttribute("passportError", "BLANK_WRONG_SYMBOLS");
        	}
        	if(!InputValidator.isValidLength(user.getPassport())){
        		isValid = false;
        		session.setAttribute("passportError", "BAD_LENGTH");
        	}
        	if(!InputValidator.isValidEmail(user.getEmail())){
        		isValid = false;
        		session.setAttribute("emailError", "WRONG_EMAIL");
        	}
        	if(!InputValidator.isValidLength(user.getEmail())){
        		isValid = false;
        		session.setAttribute("emailError", "BAD_LENGTH");
        	}        	
        }
        return isValid;
    }
    
    private void clearErrors(HttpSession session){
    	session.removeAttribute("passwordError");
    	session.removeAttribute("firstnameError");
    	session.removeAttribute("lastnameError");
    	session.removeAttribute("passportError");
    	session.removeAttribute("emailError");
    }

}
