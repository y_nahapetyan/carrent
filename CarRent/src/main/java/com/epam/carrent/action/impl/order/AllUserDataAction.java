package com.epam.carrent.action.impl.order;

import com.epam.carrent.action.ActionException;
import com.epam.carrent.action.BaseAction;
import com.epam.carrent.dao.PersistException;
import com.epam.carrent.dao.impl.DaoFactoryException;
import com.epam.carrent.dao.impl.MySqlDaoFactory;
import com.epam.carrent.dao.impl.MySqlDaoManager;
import com.epam.carrent.dao.impl.OrderDao;
import com.epam.carrent.entity.Order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import java.util.List;

public class AllUserDataAction extends BaseAction {

    private static final Logger log = Logger.getLogger(AllUserDataAction.class);
    
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ActionException {
        if (isAccessNotPermitted(request, response)) return;
        
		MySqlDaoManager daoManager = null;
        try {
        	daoManager = (MySqlDaoManager) MySqlDaoFactory.getInstance().getDaoManager();
            OrderDao orderDao = (OrderDao) daoManager.getDao(Order.class);
            
            String path = request.getServletPath();
            String status = null;
            switch (path){
                case "/all_new_orders":
                    request.getSession().setAttribute("message", "ALL_NEW_ORDERS");
                    status = "new";
                    break;
                case "/all_approved_orders":
                    request.getSession().setAttribute("message", "ALL_APPROVED_ORDERS");
                    status = "approved";
                    break;
                case "/all_rejected_orders":
                    request.getSession().setAttribute("message", "ALL_REJECTED_ORDERS");
                    status = "rejected";
                    break;
                case "/all_closed_orders":
                    request.getSession().setAttribute("message", "ALL_REJECTED_ORDERS");
                    status = "closed";
                    break;
            }
            List<Order> orders  = orderDao.getAllOrders(status);
            request.getSession().setAttribute("orders", orders);
        } catch (PersistException | DaoFactoryException e) {
            log.error("DB error", e);
            throw new ActionException("DB exception", e);
        } finally {
            try {
				daoManager.closeConnection();
			} catch (PersistException e) {
				throw new ActionException("DB exception", e);
			}
        } 
        dispatcherForward(request, response, request.getRequestDispatcher(request.getServletPath() + ".tiles"));
    }
}
