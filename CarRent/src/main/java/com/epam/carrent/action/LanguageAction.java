package com.epam.carrent.action;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Locale;

public class LanguageAction extends BaseAction {
    private static final Logger log = Logger.getLogger(LanguageAction.class);
    private static final String LANG_ID = "lang_id";
    
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ActionException {
        String language = request.getParameter(LANG_ID);
        if ((language) != null) {
            HttpSession httpSession = request.getSession();
            httpSession.setAttribute(LANG_ID, language);
            Locale locale = new Locale(language);
            Locale.setDefault(locale);
        }

        dispatchOnSamePage(request, response);
    }

    private void dispatchOnSamePage(HttpServletRequest request, HttpServletResponse response) throws ActionException {
        RequestDispatcher requestDispatcher = getSamePageDispatcher(request);
        try {
			requestDispatcher.forward(request, response);
		} catch (ServletException | IOException e) {
			log.error(e);
			throw new ActionException(e);
		} 

    }


}
