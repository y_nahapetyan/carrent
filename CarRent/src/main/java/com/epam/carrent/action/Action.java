package com.epam.carrent.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Action {
    public void execute(HttpServletRequest httpRequest, HttpServletResponse httpResponse) 
    		throws ActionException;
}
