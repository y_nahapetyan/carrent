package com.epam.carrent.action.impl.auth;

import org.apache.log4j.Logger;

import com.epam.carrent.action.ActionException;
import com.epam.carrent.action.BaseAction;
import com.epam.carrent.dao.PersistException;
import com.epam.carrent.dao.impl.DaoFactoryException;
import com.epam.carrent.dao.impl.MySqlDaoFactory;
import com.epam.carrent.dao.impl.MySqlDaoManager;
import com.epam.carrent.dao.impl.UserDao;
import com.epam.carrent.entity.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LogInAction extends BaseAction {

    private static final Logger log = Logger.getLogger(LogInAction.class);
    
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ActionException{
    	HttpSession session = request.getSession();
        User user = getUserFromParameters(request);
        cleanSessionAttributes(session);
        RequestDispatcher requestDispatcher = getSamePageDispatcher(request);
        
        boolean isAnyError = verifyUserParams(session, user);
        if (isAnyError){
            session.setAttribute("auth", true);
        } 
        dispatcherForward(request, response, requestDispatcher);
    }

    private boolean verifyUserParams(HttpSession session, User user) 
    		throws ActionException {
        boolean isAnyError = true;		
        if (checkCredentials(user)){
    		MySqlDaoManager daoManager = null;
			try {
				daoManager = (MySqlDaoManager) MySqlDaoFactory.getInstance().getDaoManager();
                UserDao userDao = (UserDao) daoManager.getDao(User.class);
                user = userDao.getByCredentials(user.getEmail(), user.getPassword());               
                if (user == null) {
                	session.setAttribute("emailError", "NO_USER_FOR_EMAIL");
                }
                else {
                	isAnyError = false;
                }
            } catch (PersistException | DaoFactoryException e) {
                log.error("DB err", e);
                throw new ActionException(e);
			} finally {
				log.info("at auth conn closed");
	            try {
					daoManager.closeConnection();
				} catch (PersistException e) {
	                log.error("DB err", e);
	                throw new ActionException("at connection close", e);
				}
	        }  
        } else {
            session.setAttribute("emailError", "BLANK_FIELDS");
        }
        session.setAttribute("user", user);
        return isAnyError;
    }

    private boolean checkCredentials(User user) {
        return user.getEmail() != null && user.getPassword()!= null &&
                !user.getEmail().isEmpty() && !user.getPassword().isEmpty();
    }

    private void cleanSessionAttributes(HttpSession session) {
        session.removeAttribute("emailError");
        session.removeAttribute("auth");
    }
}
