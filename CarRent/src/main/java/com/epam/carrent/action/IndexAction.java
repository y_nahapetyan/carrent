package com.epam.carrent.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class IndexAction extends BaseAction {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ActionException {
        String path = request.getServletPath();
        if (path.equals("/")) path = "/index";
        dispatcherForward(request, response, request.getRequestDispatcher(path +".tiles"));
    }
}
