package com.epam.carrent.action;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.epam.carrent.action.impl.auth.*;
import com.epam.carrent.action.impl.car.ReserveAction;
import com.epam.carrent.action.impl.car.SearchCarAction;
import com.epam.carrent.action.impl.order.AddPenaltyAction;
import com.epam.carrent.action.impl.order.AllUserDataAction;
import com.epam.carrent.action.impl.order.ApproveOrderAction;
import com.epam.carrent.action.impl.order.BaseViewOrderAction;
import com.epam.carrent.action.impl.order.CloseOrderAction;
import com.epam.carrent.action.impl.order.RejectOrderAction;
import com.epam.carrent.action.impl.order.UserDataAction;

import java.util.HashMap;
import java.util.Map;

public class ActionFactory {
    private static Logger logger = Logger.getLogger(ActionFactory.class);
    private static Map<String, Action> actions = new HashMap<>();
    
    static{
        actions.put("auth", new LogInAction());
        actions.put("bad", new BaseAction());
        actions.put("register", new RegistrationAction());
        actions.put("language", new LanguageAction());
        actions.put("logout", new LogOutAction());
        actions.put("index", new IndexAction());
        actions.put("searchcar", new SearchCarAction());
        actions.put("searchcar/pages", new SearchCarAction());
        actions.put("reserve", new ReserveAction());
        actions.put("my_new_orders", new UserDataAction());
        actions.put("my_rejected_orders", new UserDataAction());
        actions.put("my_approved_orders", new UserDataAction());
        actions.put("my_closed_orders", new UserDataAction());
        actions.put("all_new_orders", new AllUserDataAction());
        actions.put("all_approved_orders", new AllUserDataAction());
        actions.put("all_rejected_orders", new AllUserDataAction());
        actions.put("all_closed_orders", new AllUserDataAction());
        actions.put("order", new BaseViewOrderAction());
        actions.put("approve_order", new ApproveOrderAction());
        actions.put("reject_order", new RejectOrderAction());
        actions.put("add_penalty", new AddPenaltyAction());
        actions.put("close_order", new CloseOrderAction());
    }

    public static Action getAction(HttpServletRequest request) {
    	Action action;
        String value = request.getParameter("command");
	    logger.info(" request." + value);
    	action = actions.get(value);
    	if (action == null) {
    		String req = getActionPath(request);
    	    logger.info("request = "+req);
        	action = actions.get(req);
    	    logger.info("request = "+ action);
        	if (action == null) {
	    	    action = actions.get("index");
	    	    logger.info("Unknown request.");
        	}
    	    return action;
    	}
    	return action;
    }
    
    //todo: :- ((
    private static String getActionPath(HttpServletRequest request) {
        String[] path = request.getServletPath().split("/");
        if (path.length > 2){
            return path[1] + "/" + path[2];
        }
        if (path.length == 2)
            return path[1];
        return "";
    }
    
}
