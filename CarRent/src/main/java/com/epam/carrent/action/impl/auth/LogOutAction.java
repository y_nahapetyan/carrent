package com.epam.carrent.action.impl.auth;

import com.epam.carrent.action.Action;
import com.epam.carrent.action.ActionException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import java.io.IOException;

public class LogOutAction implements Action {
	
    private static final Logger log = Logger.getLogger(LogOutAction.class);
    
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ActionException {
        request.getSession().invalidate();
        try {
            response.sendRedirect("/index");
        } catch (IOException e) {
            log.error("Redirect", e);
            throw new ActionException(e);
        }
    }
}
