<%@include file="lib_bundle.jsp"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div style="background-image: linear-gradient( to bottom right, #3385D6 0%, #3385D6 100%);">

    <script type="text/javascript">
        function changelang(lang_id){
            document.getElementById('lang_id').value = lang_id;
            if (lang_id == "RU"){
                document.getElementById('lang_en').className = "text-muted small";
                document.getElementById('lang_ru').className = "text-primary";
            } else {
                document.getElementById('lang_ru').className =   "text-muted small";
                document.getElementById('lang_en').className =   "text-primary";
            }
            document.getElementById('lang_change').submit();
        }
    </script>

    <c:choose>
        <c:when test="${not empty lang_id && lang_id=='EN'}">
            <c:set var="classEN" value="text-primary "/>
            <c:set var="classRU" value="text-muted small"/>
        </c:when>
        <c:when test="${not empty lang_id && lang_id=='RU'}">
            <c:set var="classRU" value="text-primary "/>
            <c:set var="classEN" value="text-muted small"/>
        </c:when>
    </c:choose>
    <%--change language bar colors end--%>
    <form style="float: right"  name="lang_change" id="lang_change"  method="post" action="">
        <strong  class="${classEN}" id="lang_en" onclick="changelang('EN')">EN</strong> | <strong  class="${classRU}" id="lang_ru" onclick="changelang('RU')">RU</strong>
        <input type="hidden" name="lang_id" id="lang_id" value="">
        <input type="hidden" name="command" value="language">
    </form>

    <table>
        <tr>
            <td class="col-md-3">
            </td>
        </tr>
    </table>

</div>